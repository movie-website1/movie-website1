<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DienVienController;
use App\Http\Controllers\TheLoaiController;
use App\Http\Controllers\TrailerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::middleware('auth:api')->post('logout', [AuthController::class, 'logout']);
Route::middleware('auth:api')->get('me', [AuthController::class, 'me']);



    Route::get('/dienvien/get-all', [DienVienController::class, 'index']);
    Route::post('/dienvien/create', [DienVienController::class, 'store']);
    Route::get('/dienvien/detail/{id}', [DienVienController::class, 'show']);
    Route::put('/dienvien/update/{id}', [DienVienController::class, 'update']);
    Route::delete('/dienvien/delete/{id}', [DienVienController::class, 'destroy']);
    Route::get('/dienvien/getDienVienTheoSoPhimDong', [DienVienController::class, 'getDienVienTheoPhimDong']);

    Route::get('/trailer/get-all', [TrailerController::class, 'index']);
    Route::post('/trailer/create', [TrailerController::class, 'store']);
    Route::get('/trailer/detail/{id}', [TrailerController::class, 'show']);
    Route::put('/trailer/update/{id}', [TrailerController::class, 'update']);
    Route::delete('/trailer/delete/{id}', [TrailerController::class, 'destroy']);
    Route::get('/trailer/getTrailerPhimTheoSoLike', [TrailerController::class, 'getTrailerPhimTheoSoLike']);

    Route::get('/theloai/get-all', [TheLoaiController::class, 'index']);
    Route::post('/theloai/create', [TheLoaiController::class, 'store']);
    Route::get('/theloai/detail/{id}', [TheLoaiController::class, 'show']);
    Route::put('/theloai/update/{id}', [TheLoaiController::class, 'update']);
    Route::delete('/theloai/delete/{id}', [TheLoaiController::class, 'destroy']);
    Route::get("/theloai/danh-sach-theo-the-loai/{id}",[TheLoaiController::class,'DanhSachTheoTheLoai']);
    Route::get("theloai/danh-sach-trailer-theo-ct-the-loai/{id}",[TheLoaiController::class,'DanhSachTrailerTheoChiTietTheLoai']);
