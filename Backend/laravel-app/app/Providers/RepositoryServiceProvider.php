<?php

namespace App\Providers;

use App\Models\TheLoai;
use App\Models\Trailer;
use App\Repositories\Interface\DienVienRepositoryInterface;
use App\Repositories\Interface\GoiCuocRepositoryInterface;
use App\Repositories\Interface\NamSXRepositoryInterface;
use App\Repositories\Interface\PhimBoRepositoryInterface;
use App\Repositories\Interface\PhimLeRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Repository\QuocGiaRepository;
use App\Repositories\Interface\QuocGiaRepositoryInterface;
use App\Repositories\Interface\TheLoaiRepositoryInterface;
use App\Repositories\Interface\ThongTinCaNhanRepositoryInterface;
use App\Repositories\Interface\TrailerRepositoryInterface;
use App\Repositories\Repository\DienVienRepository;
use App\Repositories\Repository\GoiCuocRepository;
use App\Repositories\Repository\NamSXRepository;
use App\Repositories\Repository\PhimBoRepository;
use App\Repositories\Repository\PhimLeRepository;
use App\Repositories\Repository\TheLoaiRepository;
use App\Repositories\Repository\ThongTinCaNhanRepository;
use App\Repositories\Repository\TrailerRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register():void
    {

        $this->app->bind(
            GoiCuocRepositoryInterface::class,
            GoiCuocRepository::class
        );
        $this->app->bind(
            DienVienRepositoryInterface::class,
            DienVienRepository::class
        );

        $this->app->bind(
            TrailerRepositoryInterface::class,
            TrailerRepository::class
        );
        $this->app->bind(
            TheLoaiRepositoryInterface::class,
            TheLoaiRepository::class
        );
    }
}
