<?php

// app/Models/ChiTietTheLoai.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChiTietTheLoai extends Model
{
    protected $table = 'chitiettheloai';

    protected $primaryKey = 'MACHITIETTHELOAI';

    protected $fillable = [
        'MACHITIETTHELOAI',
        'MATHELOAI',
        'TENCHITIETTHELOAI',
    ];

    public function theLoai()
    {
        return $this->belongsTo(TheLoai::class, 'MATHELOAI', 'MATHELOAI');
    }
    public function trailers()
    {
        return $this->belongsToMany(Trailer::class, 'chitiettheloaiphim', 'MACHITIETTHELOAI', 'MATRAILERPHIM');
    }
}
