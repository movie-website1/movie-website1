<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThongTinCaNhan extends Model
{
    protected $table = 'thongtincanhan';
    protected $primaryKey = 'MACANHAN';
    protected $fillable = ['MACANHAN','TENCANHAN', 'NGAYSINH', 'CCCD', 'ANHDAIDIEN'];
}
