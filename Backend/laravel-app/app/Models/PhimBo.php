<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhimBo extends Model
{
    protected $table = 'phimbo';
    protected $primaryKey = 'MAPHIMBO';
    public $timestamps = false;

    protected $fillable = [
        'MAPHIMBO',
        'TENPHIMBO',
    ];
}
