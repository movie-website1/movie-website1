<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoiCuoc extends Model
{
    protected $table = 'goicuoc';
    protected $primaryKey = 'MAGOICUOC';
    public $timestamps = false;

    protected $fillable = [
        'MAGOICUOC',
        'TENGOICUOC',
        'GIAGOICCUOC',
    ];
}
