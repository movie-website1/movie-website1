<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dienvien extends Model
{
    protected $table = 'dienvien';
    public $timestamps = false;

    protected $primaryKey = 'MADIENVIEN';
    protected $fillable = ['MADIENVIEN','TENDIENVIEN', 'ANHDIENVIEN'];
    public function chitietdienviens()
    {
        return $this->hasMany(Chitietdienvien::class, 'MADIENVIEN', 'MADIENVIEN');
    }
}
