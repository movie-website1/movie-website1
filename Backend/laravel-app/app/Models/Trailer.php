<?php

// app/Models/TrailePhim.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trailer extends Model
{
    protected $table = 'trailephim';

    protected $primaryKey = 'MATRAILERPHIM';

    protected $fillable = [
        'MATRAILERPHIM',
        'TENPHIMVN',
        'MAGOICUOC',
        'VIDEO',
        'HINH',
        'THOIGIANPHIM'
    ];
    public $timestamps = false;

    public function goiCuoc()
    {
        return $this->belongsTo(GoiCuoc::class, 'MAGOICUOC', 'MAGOICUOC');
    }
    public function chiTietTheLoais()
    {
        return $this->belongsToMany(ChiTietTheLoai::class, 'chitiettheloaiphim', 'MATRAILERPHIM', 'MACHITIETTHELOAI');
    }
    public function tuongTacPhims()
    {
        return $this->hasMany(TuongTacPhim::class, 'MATRAILERPHIM', 'MATRAILERPHIM');
    }
}
