<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TheLoai extends Model
{
    protected $table = 'theloai';
    protected $primaryKey = 'MATHELOAI';
    public $timestamps = false;
    protected $fillable = [
        'MATHELOAI',
        'TENTHELOAI',
    ];
}
