<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TuongTacPhim extends Model
{
    protected $table = 'tuongtacphim';

    public $timestamps = false;

    protected $fillable = [
        'MATRAILERPHIM', 'MATRANGCANHAN'
    ];
}
