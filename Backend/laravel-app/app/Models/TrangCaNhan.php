<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrangCaNhan extends Model
{
    protected $table = 'trangcanhan';

    protected $primaryKey = 'MATRANGCANHAN';

    public $timestamps = false;

    protected $fillable = [
        'MATRANGCANHAN','MACANHAN', 'MATAIKHOAN'
    ];
}
