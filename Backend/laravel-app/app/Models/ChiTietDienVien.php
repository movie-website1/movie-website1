<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChiTietDienVien extends Model
{
    protected $table = 'chitietdienvien';

    // Không sử dụng timestamp
    public $timestamps = false;

    // Khai báo trường khóa chính nếu không phải là 'id'
    protected $primaryKey = ['MADIENVIEN', 'MATRAILERPHIM'];

    // Thiết lập các trường có thể được gán giá trị
    protected $fillable = ['MADIENVIEN', 'MATRAILERPHIM'];

    // Khai báo mối quan hệ với model Dienvien
    public function dienvien()
    {
        return $this->belongsTo(Dienvien::class, 'MADIENVIEN', 'MADIENVIEN');
    }

    // Khai báo mối quan hệ với model Trailephim
    public function trailephim()
    {
        return $this->belongsTo(Trailer::class, 'MATRAILERPHIM', 'MATRAILERPHIM');
    }
}
