<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhimLe extends Model
{
    protected $table = 'phimle';
    protected $primaryKey = 'MAPHIMLE';
    public $timestamps = false;

    protected $fillable = [
        'MAPHIMLE',
        'TENPHIMLE',
    ];
}
