<?php

namespace App\Http\Controllers;

use App\Repositories\Interface\DienVienRepositoryInterface;
use Illuminate\Http\Request;

class DienVienController extends Controller
{
    protected $goicuocRepository;

    public function __construct( DienVienRepositoryInterface $goicuocRepository)
    {
        $this->goicuocRepository = $goicuocRepository;
    }
    public function index()
    {
        $phimBos = $this->goicuocRepository->all();
        return response()->json($phimBos);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('ANHDIENVIEN')) {
            $file = $request->file('ANHDIENVIEN');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('assets/Image'), $filename);
            $data['ANHDIENVIEN'] = $filename;
        }
        $phimBo = $this->goicuocRepository->create($data);
        return response()->json($phimBo);
    }

    public function show($id)
    {
        $phimBo = $this->goicuocRepository->find($id);
        return response()->json($phimBo);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if ($request->hasFile('ANHDIENVIEN')) {
            $file = $request->file('ANHDIENVIEN');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('assets/Image'), $filename);
            $data['ANHDIENVIEN'] = $filename;
        }
        $phimBo = $this->goicuocRepository->update($id,$data);
        return response()->json($phimBo);
    }

    public function destroy($id)
    {
        $this->goicuocRepository->delete($id);
        return response()->json(['message' => 'Deleted successfully']);
    }
    public function getDienVienTheoPhimDong()
    {
        $data =$this->goicuocRepository->getDienVienTheoSoPhimDong();
        return response()->json($data);
    }
}
