<?php

namespace App\Http\Controllers;

use App\Repositories\Interface\GoiCuocRepositoryInterface;
use Illuminate\Http\Request;

class GoiCuocController extends Controller
{
    protected $goicuocRepository;

    public function __construct( GoiCuocRepositoryInterface $goicuocRepository)
    {
        $this->goicuocRepository = $goicuocRepository;
    }
    public function index()
    {
        $phimBos = $this->goicuocRepository->all();
        return response()->json($phimBos);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $phimBo = $this->goicuocRepository->create($data);
        return response()->json($phimBo);
    }

    public function show($id)
    {
        $phimBo = $this->goicuocRepository->find($id);
        return response()->json($phimBo);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $phimBo = $this->goicuocRepository->update($id, $data);
        return response()->json($phimBo);
    }

    public function destroy($id)
    {
        $this->goicuocRepository->delete($id);
        return response()->json(['message' => 'Deleted successfully']);
    }
}
