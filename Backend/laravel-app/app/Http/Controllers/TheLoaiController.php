<?php

namespace App\Http\Controllers;

use App\Repositories\Interface\TheLoaiRepositoryInterface;
use Illuminate\Http\Request;

class TheLoaiController extends Controller
{
    protected $theloaiRepository;

    public function __construct( TheLoaiRepositoryInterface $theloaiRepository)
    {
        $this->theloaiRepository = $theloaiRepository;
    }
    public function index()
    {
        $phimBos = $this->theloaiRepository->all();
        return response()->json($phimBos);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $phimBo = $this->theloaiRepository->create($data);
        return response()->json($phimBo);
    }

    public function show($id)
    {
        $phimBo = $this->theloaiRepository->find($id);
        return response()->json($phimBo);
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $phimBo = $this->theloaiRepository->update($id, $data);
        return response()->json($phimBo);
    }

    public function destroy($id)
    {
        $this->theloaiRepository->delete($id);
        return response()->json(['message' => 'Deleted successfully']);
    }
    public function DanhSachTheoTheLoai($id)
    {
       $chitiettheloais= $this->theloaiRepository->DanhSachTheoTheLoai($id);
       return response()->json($chitiettheloais);
    }
    public function DanhSachTrailerTheoChiTietTheLoai($id)
    {
        $data=$this->theloaiRepository->DanhSachTrailerTheoChiTietTheLoai($id);
        return response()->json($data);
    }
}
