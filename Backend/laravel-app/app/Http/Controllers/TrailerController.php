<?php

namespace App\Http\Controllers;

use App\Repositories\Interface\DienVienRepositoryInterface;
use App\Repositories\Interface\TrailerRepositoryInterface;
use Illuminate\Http\Request;

class TrailerController extends Controller
{
    protected $goicuocRepository;

    public function __construct(TrailerRepositoryInterface $goicuocRepository)
    {
        $this->goicuocRepository = $goicuocRepository;
    }
    public function index()
    {
        $phimBos = $this->goicuocRepository->all();
        return response()->json($phimBos);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('HINH')) {
            $file = $request->file('HINH');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('assets/Image'), $filename);
            $data['HINH'] = $filename;
        }
        $phimBo = $this->goicuocRepository->create($data);
        return response()->json($phimBo);
    }

    public function show($id)
    {
        $phimBo = $this->goicuocRepository->find($id);
        return response()->json($phimBo);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if ($request->hasFile('HINH')) {
            $file = $request->file('HINH');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('assets/Image'), $filename);
            $data['HINH'] = $filename;
        }
        $phimBo = $this->goicuocRepository->update($id, $data);
        return response()->json($phimBo);
    }

    public function destroy($id)
    {
        $this->goicuocRepository->delete($id);
        return response()->json(['message' => 'Deleted successfully']);
    }
    public function getTrailerPhimTheoSoLike()
    {
        $data=$this->goicuocRepository->TrailerPhimtheolike();
        return response()->json($data);
    }
}
