<?php

namespace App\Repositories\Repository;

use App\Models\Trailer;
use App\Models\TuongTacPhim;
use App\Repositories\BaseRepository;
use App\Repositories\Interface\TrailerRepositoryInterface;
use Illuminate\Support\Facades\DB;

class TrailerRepository extends BaseRepository implements TrailerRepositoryInterface
{
    public function __construct(Trailer $model)
    {
        parent::__construct($model);
    }
    public function TrailerPhimtheolike()
    {
        $top = Trailer::withCount('tuongtacphims')
                    ->orderBy('tuongtacphims_count', 'desc')
                    ->get();
        return $top;
    }
}
