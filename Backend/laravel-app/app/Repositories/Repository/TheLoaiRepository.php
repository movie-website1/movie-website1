<?php

namespace App\Repositories\Repository;

use App\Models\ChiTietTheLoai;
use App\Models\PhimLe;
use App\Models\TheLoai;
use App\Models\Trailer;
use App\Repositories\BaseRepository;
use App\Repositories\Interface\TheLoaiRepositoryInterface;

class TheLoaiRepository extends BaseRepository implements TheLoaiRepositoryInterface
{
    public function __construct(TheLoai $model)
    {
        parent::__construct($model);
    }
    public function DanhSachTheoTheLoai($id)
    {
        $data=ChiTietTheLoai::where('MATHELOAI',$id)->get();
        return $data;
    }
    public function DanhSachTrailerTheoChiTietTheLoai($id)
    {
        $data = Trailer::join('chitiettheloaiphim', 'chitiettheloaiphim.MATRAILERPHIM', '=', 'trailephim.MATRAILERPHIM')
        ->join('chitiettheloai', 'chitiettheloai.MACHITIETTHELOAI', '=', 'chitiettheloaiphim.MACHITIETTHELOAI')
        ->where('chitiettheloai.MACHITIETTHELOAI', $id)
        ->select('trailephim.MATRAILERPHIM', 'trailephim.TENPHIMVN', 'trailephim.VIDEO', 'trailephim.HINH', 'trailephim.THOIGIANPHIM')
        ->get();

        return $data;
    }
}
