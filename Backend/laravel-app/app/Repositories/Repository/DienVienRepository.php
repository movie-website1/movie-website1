<?php

namespace App\Repositories\Repository;

use App\Models\Dienvien;
use App\Repositories\BaseRepository;
use App\Repositories\Interface\DienVienRepositoryInterface;
class DienVienRepository extends BaseRepository implements DienVienRepositoryInterface
{
    public function __construct(Dienvien $model)
    {
        parent::__construct($model);

    }
    public function getDienVienTheoSoPhimDong()
    {
        $topDienvien = Dienvien::withCount('chitietdienviens')
                            ->orderBy('chitietdienviens_count', 'desc')
                            ->get();

        return $topDienvien;
    }
}
