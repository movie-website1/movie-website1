<?php

namespace App\Repositories\Repository;

use App\Models\GoiCuoc;
use App\Repositories\BaseRepository;
use App\Repositories\Interface\GoiCuocRepositoryInterface;

class GoiCuocRepository extends BaseRepository implements GoiCuocRepositoryInterface
{
    public function __construct(GoiCuoc $model)
    {
        parent::__construct($model);
    }
}
