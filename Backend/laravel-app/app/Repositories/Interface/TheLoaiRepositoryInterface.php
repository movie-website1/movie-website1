<?php

namespace App\Repositories\Interface;
use App\Repositories\BaseRepositoryInterface;

interface TheLoaiRepositoryInterface extends BaseRepositoryInterface
{
    public function DanhSachTheoTheLoai($id);
    public function DanhSachTrailerTheoChiTietTheLoai($id);

}
