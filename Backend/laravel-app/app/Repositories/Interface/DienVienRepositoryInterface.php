<?php

namespace App\Repositories\Interface;
use App\Repositories\BaseRepositoryInterface;

interface DienVienRepositoryInterface extends BaseRepositoryInterface
{
    public function getDienVienTheoSoPhimDong();
}
