import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/components/pages/home.vue';
import MovieDetail from  '@/components/pages/movie-detail.vue';
import MovieTrailer from '@/components/pages/movie-trailer.vue';

const routes = [
    { path: '/', component: Home },
    { path:'/detail/:id', name: 'detail', component: MovieDetail},
    {path:'/trailer/:id', name: 'trailer', component:MovieTrailer }
    // Thêm các route khác nếu cần
  ];

  
  
  const router = createRouter({
    history: createWebHistory(),
    routes
  });
  
  export default router;

