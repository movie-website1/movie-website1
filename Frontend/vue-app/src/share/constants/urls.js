const baseURL = 'http://127.0.0.1:8000';

export const THELOAI_ALL_GET_URL = baseURL+'/api/theloai/get-all';

export const TRAILER_ALL_GET_URL = baseURL+'/api/trailer/get-all';
export const TRAILER_BEST_MOVIE_URL = baseURL+'/api/trailer/getTrailerPhimTheoSoLike';
export const TRAILER_DETAIL_MOVIE_URL = baseURL + '/api/trailer/detail'

export const DIENVIEN_BEST_GET_URL = baseURL + '/api/dienvien/getDienVienTheoSoPhimDong';
