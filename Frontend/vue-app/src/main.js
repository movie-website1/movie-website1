import './assets/style.css'
import './static/fontawesome-free-6.5.2-web/css/all.min.css'


import { createApp } from 'vue';
import App from './App.vue';
import router from './router';


const app = createApp(App);
app.use(router);

app.mount('#app');