import axios from 'axios';
import { THELOAI_ALL_GET_URL } from '@/share/constants/urls.js';

class TheloaiServices {
    constructor()
    {}
    getAllTheLoai() {
        return axios.get(THELOAI_ALL_GET_URL)
            .then(response => {
              
                console.log('hi:' +response.data);
                return response.data;
            })
            .catch(error => {
                console.error('Error fetching product list:', error);
                return [];
            });
    }
    
}

export default TheloaiServices;
