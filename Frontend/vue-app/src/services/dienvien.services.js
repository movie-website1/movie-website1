import axios from 'axios';
import {DIENVIEN_BEST_GET_URL } from '@/share/constants/urls.js';

class DienvienServices {
    constructor()
    {

    }

    getBestDienVien() {
        return axios.get(DIENVIEN_BEST_GET_URL)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.error('Error fetching product list:', error);
                return [];
            });
    }

}

export default DienvienServices;
