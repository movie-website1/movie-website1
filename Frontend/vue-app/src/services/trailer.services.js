import axios from 'axios';
import { TRAILER_ALL_GET_URL, TRAILER_BEST_MOVIE_URL,TRAILER_DETAIL_MOVIE_URL } from '@/share/constants/urls.js';

class TrailerServices {
    constructor()
    {}
    getAllTrailer() {
        return axios.get(TRAILER_ALL_GET_URL)
            .then(response => {
                // Assuming the response contains an array of products
                console.log('hi1:' +response.data);
                return response.data;
            })
            .catch(error => {
                console.error('Error fetching product list:', error);
                // Returning an empty array in case of error
                return [];
            });
    }

    getBestMovie(){
        return axios.get(TRAILER_BEST_MOVIE_URL)
            .then(response => {
              
                return response.data;
            })
            .catch(error => {
                console.error('Error fetching product list:', error);
                // Returning an empty array in case of error
                return [];
            });
    }
    

    getDetailMovie(id){
        
        return axios.get(TRAILER_DETAIL_MOVIE_URL+'/' + id)
            .then(response => {
                
                return response.data;
            })
            .catch(error => {
                console.error('Error fetching product list:', error);
                // Returning an empty array in case of error
                return [];
            });
    }
}

export default TrailerServices;
